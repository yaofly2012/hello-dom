

// 
document.getElementById('caculate').onclick =  function() {

    console.group('viewport')
    console.log(window.innerWidth, window.innerHeight);
    console.log(document.documentElement.clientWidth, 
        document.documentElement.clientHeight, 
        document.documentElement.clientTop,
        document.documentElement.clientLeft);
    console.groupEnd('viewport');

    var rect = document.getElementsByClassName('box')[0].getClientRects();
    console.log(rect[0])
    console.log(isElementInViewport(document.getElementsByClassName('box')[0]))
}


function isElementInViewport(el) {
    var rect = el.getBoundingClientRect();
    var innerWidth = window.innerWidth || document.documentElement.clientWidth;
    var innerHeight = window.innerHeight || document.documentElement.clientHeight;
    return rect.top < innerHeight && rect.left < innerWidth;
}