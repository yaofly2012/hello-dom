function timeoutPromise(ms) {
    return new Promise((resolve, reject) => {
        setTimeout(reject, ms)
    })
}

function fetchTimeout(input, init) {
    const { timeout = 3000, ...restInit } = init || {};
    return Promise.race(fetch(input, restInit), timeoutPromise(timeout));
}

function cancelablePromise(promise) {
    var rejectHandle;
    var cancellabelPromise = new Promise((resolve, reject) => {
        rejectHandle = () => {
            reject(new TypeError('Canceled by user', 'CanceledError'))
        };
    })

    var r = Promise.race([cancellabelPromise, promise]);
    r.cancel = rejectHandle;
    return r;
}

function cancelablePromise(promise) {
    var rejectHandle;
    var r = new Promise((resolve, reject) => {         
        rejectHandle = () => {
            reject(new TypeError('Canceled by user'))
        }          
        promise.then(resolve, reject);
    })
    r.cancel = rejectHandle;
    return r;
}