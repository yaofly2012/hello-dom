const express = require("express");
const multiparty = require('multiparty');
const app = express();
const port = 8088;

app.get('/a', (req, res) => {    
    res.end(`
    <!DOCTYPE html>
    <html>
        <head></head>
        <body>
            <h1>Performance</h1>
            <a href="http://localhost:8088/b">Redirect</a>     
            <script type="text/javascript">
            ;(function() {
                function sleep(delay) {
                    var now = Date.now();
                    delay = delay || 500;
                    while(Date.now() - now < delay) {}
                }

                window.onbeforeunload = function(e) {
                    console.log('onbeforeunload');
                    e.preventDefault();
                    e.returnValue = '';
                    sleep();
                }

                window.onunload = function(e) {
                    console.log('onunload');
                    e.preventDefault();
                    e.returnValue = 'hello';
                    sleep(5000);
                }
            })()
        </script>
        </body>
    </html>
    `);
})

app.get('/b', (req, res) => {
    res.setHeader('location', 'http://localhost:8088/c');
    res.status(302);
    var now = Date.now();
    while(Date.now() - now < 200) {}
    res.end('Redirect');
})

app.get('/c', (req, res) => {
    res.setHeader('location', 'http://localhost:8088/d');
    res.status(302);
    var now = Date.now();
    while(Date.now() - now < 300) {}
    res.end('Redirect');
})

app.get('/d', (req, res) => {
    res.setHeader('keep-alive', true);
    res.end(`
        <!DOCTYPE html>
        <html>
            <head></head>
            <body>
                <h1>Detail</h1>
                <script type="text/javascript">
                    ;(function() {
                        var timing = window.performance.timing;
                        console.log("navigationStart", timing.navigationStart);     
                        console.log("unloadEventStart", timing.unloadEventStart);
                        console.log("unloadEventEnd", timing.unloadEventEnd);
                        console.log("redirectStart", timing.redirectStart)
                        console.log("redirectEnd", timing.redirectEnd)
                        console.log("fetchStart", timing.fetchStart)
                        console.log("domainLookupStart", timing.domainLookupStart)
                        console.log("domainLookupEnd", timing.domainLookupEnd)
                        console.log("connectStart", timing.connectStart)
                        console.log("connectEnd", timing.connectEnd)
                        
                        console.log("requestStart", timing.requestStart)
                        console.log("responseStart", timing.responseStart)
                        console.log("responseEnd", timing.responseEnd)

                        console.log("domLoading", timing.domLoading)
                    })()
                </script>
            </body>
        </html>
    `);
})

// app.post('/formData', (req, res) => {
//     res.writeHead(200, { 'content-type': 'text/plain' });
//     res.write('received upload:\n\n');
//     res.end();
//     // var form = new multiparty.Form();
 
//     // form.parse(req, function(err, fields, files) {
//     //     if(!!err) {
//     //         console.log(err)
//     //         res.end(err);
//     //         return;
//     //     }
//     //     console.log(fields)
//     //     res.writeHead(200, { 'content-type': 'text/plain' });
//     //     res.write('received upload:\n\n');
//     //     res.end();
//     //     //res.json({ fields: fields, files: files });
//     // });
// })

app.all('/', (req, res) => {
    console.log('Begin process req')
    res.setHeader('Access-Control-Allow-Origin', req.headers['origin']);
    res.setHeader('Access-Control-Allow-Credentials', true); 
    
    console.log(`req.body`, req.body);

    try {
        if(req.method === 'OPTIONS') {
            var accessControlRequestMethod = req.get('Access-Control-Request-Method');
            var accessControlRequestHeaders = req.get('Access-Control-Request-Headers');
            console.log(`Access-Control-Allow-Method=${accessControlRequestMethod}`);
            console.log(`Access-Control-Allow-Headers=${accessControlRequestHeaders}`);
    
            if(accessControlRequestMethod) {
                res.setHeader('Access-Control-Allow-Method', accessControlRequestMethod);
            }        
            if(accessControlRequestHeaders) {
                res.setHeader('Access-Control-Allow-Headers', accessControlRequestHeaders);
            }
        } else {   
            res.setHeader('x-page-id', 'abc')
            res.setHeader('x-pagetrace', 'hello')
            res.setHeader('Access-Control-Expose-Headers', 'x-page-id');
            res.cookie('_test_', 'yao');
            res.on('data', function(chunk) {
                console.log(`data chunk`, chunk)
            })
        }
    } catch(e) {
        console.log(e);
    } finally {    
        res.send('hello World');
        console.log('End process req')
    }
})

app.listen(port, () => {
    console.log(`Server listening at port ${port}`);
})